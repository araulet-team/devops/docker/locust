from locust import HttpLocust
from locust import TaskSet
from locust import task


class HelloTaskSet(TaskSet):

    @task
    def my_task(self):
        self.client.get('https://reqres.in/api/users?page=2')


class HelloLocust(HttpLocust):
    task_set = HelloTaskSet



