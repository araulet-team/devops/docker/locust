FROM alpine:latest

LABEL maintainer="a.raulet93@gmail.com"
LABEL description="Locust is an easy-to-use, distributed, user load testing tool."

ARG RELEASE_LOCUST="${RELEASE_LOCUST:-0.10.0}"
ENV RELEASE_LOCUST="${RELEASE_LOCUST}"

RUN apk add --no-cache ca-certificates python3 python3-dev build-base libzmq musl-dev zeromq-dev \
    && python3 -m ensurepip --upgrade \
    && if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi \
    && if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi \
    && pip install --no-cache-dir locustio==${RELEASE_LOCUST} pyzmq \
    && apk del build-base musl-dev python3-dev zeromq-dev

EXPOSE 8089 5557 5558

ENTRYPOINT ["/usr/bin/locust"]

CMD ["--help"]
