# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.1.0
2019-10-19

### Features

- locust 0.11.0 + new ci (455074ffbac483588d0a9790a637556d449ed85f)

### Fixes

- **ci:** docker tags (3cb7a997d92498da9d0ae62dcf1cd220f69895b5)

